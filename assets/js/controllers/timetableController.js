'use strict';

define([
], function () {
    return ['$scope', '$routeParams', '$location', '$q', 'GoogleAuthService', 'TimetableService', 
    function ($scope, $routeParams, $location, $q, gAuth, timetable) {
        $scope.weeks = [];
        $scope.week = null;

        $scope.authorized = false;
        $scope.loading = true;
        
        // функция авторизации в Google OAuth
        $scope.authorize = function (immediate) {
            immediate = immediate || false;
            gAuth.authorize(immediate).then(
                function() {
                    $scope.authorized = true;
                    $scope.loading = true;
                    
                    return timetable.getWeeks();
                }, 
                function () {
                    $scope.loading = false;
                    $scope.authorized = false;
                    return $q.reject();
                }
            ).then(
                function(weeks) {
                    $scope.weeks = weeks;
                    
                    if ($routeParams.week) {
                        $scope.date = new Date($routeParams.week);
                    } else {
                        $scope.date = new Date();
                    }
                }, 
                function(error) {
                    showError(error, "Не удалось получить список рабочих недель");
                }
            );
        };
        
        // загружает смены для указанной недели
        $scope.$watch('week', function(week) {
            if (!week) return;
            
            $scope.loading = true;
            
            // загружаем список сотрудников из графика
            week.getEmployees().then(
                function(employees) {
                    return week.loadTimetable();
                },
                function(error) {
                    showError(error, "Не удалось получить список сотрудников для текущей недели");
                    return $q.reject(error);
                }
            ).then(
                function(timetable) {
                    // если мы обратидись по адресу /timetable/:name
                    if ($routeParams.name) {
                        var employees = week.employees.filter(function(emp) {
                            return emp.name === $routeParams.name;
                        });
                        
                        if (employees.length > 0) {
                            $scope.employee = employees.pop();
                        }
                    }
                    
                    $scope.loading = false;
                }, 
                function(error) {
                    showError(error, "Не удалось найти график смен для текущей недели");
                    return $q.reject(error);
                }
            );
        });
        
        // обновляем URL в браузере при выборе сотрудника из списка
        $scope.$watchGroup(['employee', 'week'], function(values) {
            var employee = values[0];
            var week = values[1];
            
            if (employee && employee.name && week) {
                var date = week.monday.toJSON().substring(0, 10);
                $location.path('/timetable/' + employee.name + '/' + date, false);
                $routeParams.name = employee.name;
                $routeParams.week = date;
            }
        });
        
        // обновляем week при изменении date
        $scope.$watch('date', function(date) {
            if (!date || !$scope.weeks.length) return;
            
            var weeks = $scope.weeks.filter(function(item) { return item.contains(date); });
            if (weeks.length === 0) {
                var error = { 
                    error: 1,
                    message: "Не удалось найти страницу с текущей неделей"
                };
                showError(error, error.message);
                return;
            }
                    
            if (weeks.length > 1) {
                console.warn("Найдено больше одной недели, содержащей текущую дату!");
                console.debug(weeks);
            }
            
            $scope.week = weeks[0];
        });
      
        // пытаемся выполнить авторизацию в фоне (immediate = true)
        $scope.authorize(true);
        
        // загружает смены на неделю назад от текущей недели
        $scope.prevWeek = function() {
            $scope.date = $scope.week.prev.monday;
        };
        
        // загружает смены на неделю вперёд от текущей недели
        $scope.nextWeek = function() {
            $scope.date = $scope.week.next.monday;
        };
        
        // переводит состояние контроллера для отобрадения ошибки в UI
        function showError(error, message, debug) {
            $scope.loading = false;
            $scope.error = error;
            
            console.warn(message);
            if (debug !== undefined) console.debug(debug);
        }
    }];
});
