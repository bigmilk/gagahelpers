require.config({
    baseUrl: './assets/js',
    paths: {
        bootstrap: '../vendors/bootstrap/dist/js/bootstrap',
        holderjs: '../vendors/holderjs/holder',
        html5shiv: '../vendors/html5shiv/dist/html5shiv',
        requirejs: '../vendors/requirejs/require',
        respond: '../vendors/respond/dest/respond.src',
        zeroclipboard: '../vendors/zeroclipboard/dist/ZeroClipboard',
        domReady: '../vendors/domReady/domReady',
        jquery: '../vendors/jquery/dist/jquery',
        x2js: '../vendors/x2js/xml2json.min',
        angular: '../vendors/angular/angular',
        'angular-bootstrap': '../vendors/angular-bootstrap/ui-bootstrap-tpls',
        'angular-route': '../vendors/angular-route/angular-route',
        'angular-xml': '../vendors/angular-xml/angular-xml',
        'angular-i18n_ru-ru': '../vendors/angular-i18n/angular-locale_ru-ru'
    },
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            deps: [
                'jquery'
            ]
        },
        angular: {
            exports: 'angular'
        },
        'angular-route': {
            deps: [
                'angular'
            ]
        },
        'angular-bootstrap': {
            deps: [
                'angular'
            ]
        },
        'angular-xml': {
            deps: [
                'angular',
                'x2js'
            ]
        },
        'angular-i18n_ru-ru': {
            deps: [
                'angular'
            ]
        }
    },
    priority: [
        'angular'
    ],
    packages: [

    ],
    deps: [
        './main'
    ]
});
