'use strict';

define(function(require) {
    require('angular').module('gaga.helpers.services', [])
        .service( 'GoogleAuthService', require('services/googleAuthService') )
        .service( 'GoogleSheetsService', require('services/googleSheetsService') )
        .service( 'TimetableService', require('services/timetableService') );
});