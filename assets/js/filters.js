'use strict';

define(function(require) {
    require('angular').module('gaga.helpers.filters', [])
        .filter( 'capitalize', require('filters/capitalize') )
        .filter( 'numberFixedLen', require('filters/numberFixedLen') );
});