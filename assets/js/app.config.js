'use strict';

define(['app'], function (app) {
    return app
        .config(function ($httpProvider, $locationProvider) {
            $httpProvider.interceptors.push('xmlHttpInterceptor');
            $locationProvider.html5Mode(true);
        })
        .run(['$rootScope', '$location', '$route', function($rootScope, $location, $route) {
            $rootScope.isCurrentPath = function(path) {
                if (path.charAt(0) != '/') path = '/' + path;
                return $location.path() === path;
            };

            $rootScope.$on('$routeChangeSuccess', function (event, current) {
                if (!current.$$route) return;
                $rootScope.title = current.$$route.title;
            });
            
            var original = $location.path;
            $location.path = function (path, reload) {
                if (reload === false) {
                    var lastRoute = $route.current;
                    var un = $rootScope.$on('$locationChangeSuccess', function () {
                        $route.current = lastRoute;
                        un();
                    });
                }
                return original.apply($location, [path]);
            };
        }]);
});
