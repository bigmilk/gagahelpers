'use strict';

require([
    'angular',
    'angular-route',
    'angular-bootstrap',
    'angular-xml',
    'angular-i18n_ru-ru',
    'app',
    'app.config',
    'app.routes',
    'controllers',
    'services',
    'directives',
    'filters'
], function () {
    angular.bootstrap( document, ['gaga.helpers'] );

});