'use strict';

define(function() { return function() { return {
    transclude: true,
    templateUrl: 'assets/partials/weekday.html',
    scope: {
        time: '=date'
    },
    controller: function($scope) {
        // из-за того, что Date.getDay() возвращает 0 для восресенья
        // описываем массив с русскими названиями от воскресенья
        var weekdayNames = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        
        // следим за изменением переменной date,
        // чтобы обновлять имя дня недели
        $scope.$watch('time', function(date) {
            $scope.name = weekdayNames[date.getDay()];
        });
        
        // сравнивает переменную time с текущей датой без учёта времени
        $scope.isCurrentDate = function () {
            var date = $scope.time;
            if (!date) return false;
            
            var now = new Date();
            return now.getFullYear() == date.getFullYear()
                && now.getMonth() == date.getMonth()
                && now.getDate() == date.getDate();
        };
    }
}}});