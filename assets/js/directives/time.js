'use strict';

define(function() { return function() { return {
    scope: {
        time: '=date'
    },
    template: "{{time | date : 'H'}}<sup>{{time | date : 'mm'}}</sup>"
}}});