
'use strict';

define(function() { return function() { return {
    templateUrl: 'assets/partials/shift.html',
    scope: {
        shift: '='
    },
    controller: ['$scope', 'TimetableService', function($scope, timetable) {
        timetable.getWeeks().then(function(weeks) {
            $scope.$watch('shift', function(shift) {
                if (!shift) return;
                
                // дата смены
                var date = shift.date;
                
                weeks = weeks.filter(function(week) {
                    return week.contains(date);
                });
                
                if (weeks.length === 0) {
                    console.warn("Директива 'shift' не смогла найти неделю, содержащую дату " + date + ".");
                    return;
                }
                
                var week = weeks[0];
                
                // предполагается, что данные о сотрудниках для нужной недели уже загружены
                if (week.employees.length === 0) {
                    console.warn("Директива 'shift' ожидала загруженные данные, но список сотрудников пуст.");
                    return;
                }
                
                // получаем список сотрудников, которые 
                // работают в этот же день в этом же магазине
                var employees = week.getEmployeesForShopShift(date, shift.shop);
                
                // убираем из списка сотрудников того, чья смена сейчас в $scope
                $scope.team = employees.filter(function(emp) {
                    return emp.getShift(date, shift.shop) != shift;
                });
            });
        });
    }]
}}});