'use strict';

define(function() { return function() { return {
    templateUrl: 'assets/partials/team.html',
    scope: {
        team: '=',
        shiftDate: '=date',
        shop: '=shop'
    }
}}});