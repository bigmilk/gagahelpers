'use strict';

define(['app'], function (app) {
    return app.config( ['$routeProvider', function( $routeProvider ) {
        $routeProvider.when('/timetable', {
            templateUrl: 'assets/partials/timetable.html',
            controller: 'TimetableController',
            title: 'График'
        });
        
        $routeProvider.when('/timetable/:name', {
            templateUrl: 'assets/partials/timetable.html',
            controller: 'TimetableController',
            title: 'График'
        });
        
        $routeProvider.when('/timetable/:name/:week', {
            templateUrl: 'assets/partials/timetable.html',
            controller: 'TimetableController',
            title: 'График'
        });

        $routeProvider.when('/features', {
            templateUrl: 'assets/partials/features.html',
            controller: 'FeaturesController',
            title: 'Возможности'
        });

        $routeProvider.when('/contacts', {
            templateUrl: 'assets/partials/contacts.html',
            controller: 'ContactsController',
            title: 'Контакты'
        });

        $routeProvider.otherwise({
            redirectTo: '/timetable'
        });
    }]);
});