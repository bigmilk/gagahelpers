'use strict';

define(function() { return function() { return function(input) {
    return (!!input) ? input.replace(/([\wа-я_]+[^\s-]*) */g, function(txt) { 
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }) : '';
}}});