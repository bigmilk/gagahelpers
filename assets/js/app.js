'use strict';

define(['angular'], function () {
    return angular.module('gaga.helpers', [
        'ngRoute',
        'ngLocale',
        'ui.bootstrap',
        'xml',
        'gaga.helpers.services',
        'gaga.helpers.controllers',
        'gaga.helpers.directives',
        'gaga.helpers.filters'
    ]);
});