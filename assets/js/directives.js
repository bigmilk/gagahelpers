'use strict';

define(function(require) {
    require( 'angular' ).module( 'gaga.helpers.directives', [] ).
        directive( 'gagaTime', require( 'directives/time' )).
        directive( 'gagaWeekday', require( 'directives/weekday' )).
        directive( 'gagaShift', require( 'directives/shift' )).
        directive( 'gagaTeam', require( 'directives/team' ));
});