'use strict';

define(function(require) {
    require( 'angular' ).module( 'gaga.helpers.controllers', [ 'gaga.helpers.services' ] ).
        controller( 'TimetableController', require( 'controllers/timetableController' )).
        controller( 'FeaturesController', require( 'controllers/featuresController' )).
        controller( 'ContactsController', require( 'controllers/contactsController' ) );
});