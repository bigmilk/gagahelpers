'use strict';

define(function() {
    return function($q) {
        var API_KEY = 'AIzaSyAgA7oJ0xGGZ210KQaZXNUSUq7H6f1Ry5o';
        var CLIENT_ID = '237156947989-p692jpiimf8ua46trb7fsuees34s7a01.apps.googleusercontent.com';
        var SCOPES = [
            //		'https://www.googleapis.com/auth/drive.readonly',
            //		'https://docs.google.com/feeds',
            //		'email',
            'profile',
            'https://spreadsheets.google.com/feeds'
        ];

        return {
            authorize: function (immediate) {
                immediate = immediate || false;
                
                return $q(function(resolve, reject) {
                    gapi.client.setApiKey(API_KEY);
                    gapi.auth.authorize({
                        'client_id': CLIENT_ID,
                        'scope': SCOPES.join(' '),
                        'immediate': immediate
                    }).then(resolve, reject);
                })
            },

            headers: function () {
                return gapi.auth.getToken() ? {
                    'Authorization': gapi.auth.getToken().token_type + ' ' + gapi.auth.getToken().access_token
                } : null;
            }
        };
    }
});