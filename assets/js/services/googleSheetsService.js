'use strict';

define(['angular', 'jquery'], function() {
    // объект ошибки
    function GoogleSheetsError(code, message) {
        this.code = code;
        this.message = message;
    }
    
    // возвращает позицию ячейки по её ID
    function getCellPosition(id) {
        var res = /R(\d+)C(\d+)$/.exec(id);
        if (!res || res.length < 3) return null;
        return {row: parseInt(res[1]), col: parseInt(res[2])}
    }

    return ['$http', '$q', 'GoogleAuthService', function($http, $q, gAuth) {
        return {
            // возвращает информацию о таблице с описанием её страниц
            getSpreadsheet: function (key) {
                if (gAuth.headers() === null) {
                    return $q.reject(new GoogleSheetsError(1, 'Not authorized in Google'));
                }
                    
                return $http.get('http://bigmilk.ru/proxy.php', {
                    headers: gAuth.headers(),
                    params: {
                        url: 'https://spreadsheets.google.com/feeds/worksheets/' + key + '/private/basic?v=3.0'
                    }
                }).then(
                    function(response) {
                        var data = response.data;
                        if (!data.hasOwnProperty('feed')) {
                            return $q.reject(new GoogleSheetsError(2, 'Wrong spreadsheets response. Feed not found.'));
                        }

                        var feed = data.feed;
                        var spreadsheet = {
                            id: key,
                            title: feed.title.toString(),
                            author: feed.author,
                            updated: new Date(feed.updated),
                            worksheets: []
                        };

                        feed.entry.forEach(function(entry) {
                            spreadsheet.worksheets.push({
                                title: entry.title.toString(),
                                content: entry.content.toString(),
                                id: key + '/' + entry.id.substr(entry.id.lastIndexOf('/') + 1),
                                updated: entry.updated
                            });
                        });

                        return spreadsheet;
                    },
                    function(data) {
                        return $q.reject(
                            new GoogleSheetsError(data.status, 'HTTP request error. HTTP status code: ' + data.status));
                    }
                );
            },
            
            // возвращает информацию о странице с массивом ячеек,
            // соотвествующих опциям options
            // допустимые опции: https://developers.google.com/google-apps/spreadsheets/#fetching_specific_rows_or_columns
            getWorksheet: function (key, options) {
                return $q(function(resolve, reject) {
                    options = angular.extend({v: '3.0'}, options);
                    
                    if (gAuth.headers() === null) {
                        return reject(new GoogleSheetsError(1, 'Not authorized in Google'));
                    }
    
                    return $http.get('http://bigmilk.ru/proxy.php', {
                        headers: gAuth.headers(),
                        params: {
                            //mode: 'native',
                            url: 'https://spreadsheets.google.com/feeds/cells/' + key + '/private/basic?' + jQuery.param(options)
                        }
                    }).then(
                        function (response) {
                            var data = response.data;
                            if (!data.hasOwnProperty('feed')) {
                                reject(new GoogleSheetsError(1, 'Wrong response'));
                            }
    
                            var feed = data.feed;
                            var spreadsheet = {
                                id: key,
                                title: feed.title.toString(),
                                author: feed.author,
                                updated: new Date(feed.updated),
                                cells: []
                            };
    
                            if (feed.hasOwnProperty('entry')) {
                                feed.entry.forEach(function(cell) {
                                    spreadsheet.cells.push({
                                        title: cell.title.toString(),
                                        content: cell.content.toString(),
                                        id: key + '/' + cell.id.substr(cell.id.lastIndexOf('/') + 1),
                                        updated: cell.updated,
                                        position: getCellPosition(cell.id)
                                    });
                                });
                            }
    
                            return resolve(spreadsheet);
                        },
                        function (data) {
                            return reject(
                                new GoogleSheetsError(data.status, 'HTTP request error. HTTP status code: ' + data.status));
                        }
                    );
                });
            }
        };
    }];
});