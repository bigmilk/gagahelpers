'use strict';

define(function() {
    var documentKey = '0AmHs4vvLPqrudHA2YjAxUGxkaGZRNlhOai1MWU9SdHc';
    var employees = [];
    var weeks = [];
    
    // создаёт дату из строки формата dd.mm.yyyy
    // возвращает объект Date или null
    function parseDate(dateString) {
        var dateParts = /(\d+)[^\d]+(\d+)[^\d]+(\d+)/.exec(dateString);
        if (!dateParts || dateParts.length < 4) return null;
        
        var day = parseInt(dateParts[1], 10);
        var month = parseInt(dateParts[2], 10) - 1;
        var year = parseInt(dateParts[3], 10);
        
        // валидируем данные
        if (day < 1 || day > 31) return null;
        if (month < 0 || month > 11) return null;
            
        // если год состоит из 2-х чисел
        if (year < 100) {
            year += year > 80 ? 1900 : 2000;
        }
        
        // если год меньше 1990 или больше текущего + 1
        if (year < 1900 || year > (new Date()).getUTCFullYear() + 1) return null;
        
        var date = new Date();
        date.setUTCFullYear(year);
        date.setUTCMonth(month);
        date.setUTCDate(day);
        date.setUTCHours(0);
        date.setUTCMinutes(0);
        date.setUTCSeconds(0);
        date.setUTCMilliseconds(0);
        
        return date;
    }
    
    return ['$http', '$q', 'GoogleSheetsService', function($http, $q, gSheets) {
        // объект ошибки
        function TimetableError(code, message) {
            this.code = code;
            this.message = message;
        }
        
        // конструктор для класса мены сотрудника
        function EmployeeShift(date, shop, offset, duration, sick) {
            var _date, _offset, _duration, _beginDate, _endDate;
            
            // длина полной смены - 12 часов, т.е 720 минут
            var fullDuration = 720;
            
            function updateBeginDate() {
                if (!this.date || this.offset === undefined) return;
                
                // высчитываем смещение в минутах от начала рабочего дня
                var offset = this.offset * fullDuration;
                
                // часы и минуты начала смены (10 - начало рабочего дня)
                var hours = 10 + Math.floor(offset / 60);
                var minutes = Math.floor(offset % 60);
                
                _beginDate = new Date(this.date);
                _beginDate.setHours(hours);
                _beginDate.setMinutes(minutes);
                
                updateEndDate.call(this);
            }
            
            function updateEndDate() {
                if (!this.beginDate || this.duration === undefined) return;
                
                // высчитываем длину смены в минутах
                var duration = Math.floor(this.duration * fullDuration);
                
                _endDate = new Date(this.beginDate);
                _endDate.setMinutes(this.beginDate.getMinutes() + duration);
                
                // исправляем время типа 21:59 на 22:00
                if (_endDate.getMinutes() == 59) {
                    _endDate.setMinutes(_endDate.getMinutes() + 1);
                }
            }
            
            Object.defineProperty(this, 'date', {
                enumerable: true,
                get: function() { return _date; },
                set: function(val) { 
                    _date = new Date(val);
                    updateBeginDate.call(this);
                }
            });
            
            Object.defineProperty(this, 'offset', {
                enumerable: true,
                get: function() { return _offset; },
                set: function(val) { 
                    _offset = val;
                    updateBeginDate.call(this);
                }
            });
            
            Object.defineProperty(this, 'duration', {
                enumerable: true,
                get: function() { return _duration; },
                set: function(val) { 
                    _duration = val;
                    updateEndDate.call(this);
                }
            });
            
            Object.defineProperty(this, 'beginDate', {
                enumerable: true,
                get: function() { 
                    return _beginDate;
                }
            });
            
            Object.defineProperty(this, 'endDate', {
                enumerable: true,
                get: function() { 
                    return _endDate;
                }
            });
            
            switch (shop) {
                case 'бу':
                    shop = 'Семинар «Банда Умников»';
                    break;
                    
                case 'петр':
                    shop = 'Магазин «Петроградская»';
                    break;
                    
                case 'нев':
                    shop = 'Магазин «Маяковская»';
                    break;
                    
                case 'кон':
                    shop = 'Магазин «Конюшенная»';
                    break;
                    
                case 'сад':
                    shop = 'Магазин «Садовая»';
                    break;
                    
                case 'дыб':
                    shop = 'Магазин «Дыбенко»';
                    break;
                    
                case 'васька':
                    shop = 'Магазин «Василеостровская»';
                    break;
                    
                case 'пио':
                    shop = 'Магазин «Пионерская»';
                    break;
                    
                case 'межд':
                    shop = 'Магазин «Международная»';
                    break;
                    
                case 'свет':
                    shop = 'Магазин «Просвещения»';
                    break;
                    
                case 'моск':
                    shop = 'Магазин «Московская»';
                    break;
            }
            
            this.date = new Date(date);
            this.offset = offset;
            this.duration = duration;
            this.shop = shop;
            this.sick = sick || false;
        }
        
        // конструктор для класса с описанием сотрудника
        function Employee(name, id) {
            this.name = name;
            this.id = id;
            this.timetable = [];
            this.timetableIndex = [];
        }
        
        // добавляет рабочую смену в график сотрудника
        Employee.prototype.setShift = function(date, text) {
            var shifts = [];
            var sick = text === 'б';
            
            if (sick) {
                var shift = new EmployeeShift(date, undefined, undefined, sick);
                shift.text = text;
                shifts.push(shift);
            } else {
                var texts = text.split('+');
                
                var emp = this;
                var lastShift = null;
                texts.forEach(function(text) {
                    var durationMatch = /(\d+\.\d+)/.exec(text);
                    var duration = parseFloat(durationMatch) || (texts.length > 1 ? undefined : 1);
                    var shop = text.replace(/\d+\.\d+| /g, '');
                    
                    // определяем смещение смены
                    // в стандарном случае смещение = 1 - duration
                    var offset = 1 - (duration || 1);
                    
                    // но если смен несколько
                    if (texts.length > 1) {
                        // и если это первая, смещение = 0
                        if (shifts.length === 0) {
                            offset = 0;
                        } else {
                            // если это не первая и предыдущая смена имеет длину
                            if (lastShift.duration) {
                                // тогда смещение = смещение предыдущей + длина предыдущей
                                offset = lastShift.offset + lastShift.duration;
                            } else {
                                // если же длина предыдущей не указана,
                                // высчитываем начало от конца
                                offset = 1 - duration;
                                
                                // и устанавливаем длину предыдущей до начала текущей
                                lastShift.duration = offset;
                            }
                        }
                    }
                    
                    var shift = new EmployeeShift(date, shop, offset, duration, sick);
                    shift.text = text;
                    
                    shifts.push(shift);
                    lastShift = shift;
                });
            }
            
            this.timetable.push(shifts);
            
            // обновляем индекс смен по timestamp
            this.timetableIndex = this.timetable.map(function(shift) {
                return shift[0].date.getTime();
            });
        };
        
        // получаем рабочие смены из графика сотрудника для указанных дня
        // и магазина (если указан)
        Employee.prototype.getShift = function(date, shop) {
            var index = this.timetableIndex.indexOf(date.getTime());
            if (index == -1) return undefined;
            
            var shifts = this.timetable[index];
            
            // если магазин не указан, возвращаем массив смен
            if (!shop) return shifts;
            
            // иначе фильтруем по имени магазина
            shifts = shifts.filter(function(shift) {
                return shift.shop === shop;
            });
            
            return shifts.length === 0 ? undefined : shifts[0];
        };
        
        // конструктор для класса с описанием недели
        function Week(worksheet, monday) {
            this.worksheet = worksheet;
            this.employees = [];
            this.prev = undefined;
            this.next = undefined;
            this.timetableLoaded = false;
            this.monday = monday instanceof Date ? new Date(monday) : new Date();
            
            // обнуляем текущее время для понедельника (00:00:00:000)
            this.monday.setUTCHours(0);
            this.monday.setUTCMinutes(0);
            this.monday.setUTCSeconds(0);
            this.monday.setUTCMilliseconds(0);
            
            // создаём дату для воскресенья
            // эта дата будет использоваться для определения крайнего времени недели
            this.sunday = new Date(this.monday);
            this.sunday.setDate(this.monday.getDate() + 6);
            this.sunday.setUTCHours(23);
            this.sunday.setUTCMinutes(59);
            this.sunday.setUTCSeconds(59);
            this.sunday.setUTCMilliseconds(999);
            
            // вычисляем начальный номер столбца в таблице графика
            var date = parseDate(this.worksheet.title);
            if (date === null) 
                throw new SyntaxError("Неожиданный формат даты в заголовке листа: '" + this.worksheet.title + "'");
            
            
            // если дата в имени документа совпадает с датой понедельника
            // тогда это номер столбца понедельника = 3
            if (date.getTime() == this.monday.getTime()) this.mondayIndex = 3;
            else {
                date.setDate(date.getDate() + 7);
                // иначе дата + 7 дней должна совпадать с датой понедельника
                // и тогда номер столбца = 11
                if (date.getTime() == this.monday.getTime()) this.mondayIndex = 11;
                // иначе или неверно указана дата начала недели или лист
                else throw new ReferenceError("Не совпадают даты начала недели и даты листа '" + this.worksheet.title + "'");
            }
        }
        
        // возвращает истину, если дата принадлежит этой неделе
        Week.prototype.contains = function(date) {
            return date >= this.monday && date <= this.sunday;
        };
        
        // возвращает список дней на этой неделе
        // у каждого дня время обнулено
        Week.prototype.getDays = function() {
            if (this.days) return this.days;
            
            var tmp = new Date(this.monday);
            var days = [];
            for(var i=0; i < 7; i++) {
                days.push(new Date(tmp));
                tmp.setDate(tmp.getDate() + 1);
            }
            
            this.days = days;
            return days;
        };
        
        // получает список сотрудников для указанной недели
        // возвращает HttpPromise
        Week.prototype.getEmployees = function() {
            var week = this;
            
            // если список сотрудников уже загружен, просто отдаём его
            if (week.employees.length) {
                return $q.when(week.employees);
            }
                
            // запрашиваем список сотрудников
            return gSheets.getWorksheet(week.worksheet.id, {'min-col': 1, 'max-col': 1}).then(
                function (result) {
                    // создаём список сотрудников
                    result.cells.forEach(function (cell, i) {
                        // пропускаем первые 5 - заголовки таблицы
                        if (i < 5) return true;
                        
                        // пропускаем некорректные заголовки
                        if (cell.content.toLowerCase() === 'команды') return true;
                        if (cell.content.length === 0) return true;
        
                        week.employees.push(new Employee(cell.content, cell.position.row));
                    });
                    
                    return week.employees;
                }
            );
        };
        
        // возвращает список сотрудниковЮ работающих в магазине shop
        // в указанную дату date
        Week.prototype.getEmployeesForShopShift = function(date, shop) {
            var employees = [];
            this.employees.forEach(function(emp) {
                var shift = emp.getShift(date, shop);
                if (!shift) return;
                if (shift.shop != shop) return;
                
                employees.push(emp);
            });
            
            return employees;
        };
        
        // загружает график смен для всех сотрудников для указанной недели
        // в отличии от метода Employee.getTimetable, выполняет один запрос
        // вместо n запросов, равных количеству сотрудников,
        // которые понадобились бы для загрузки графика для всех сотрудников
        // при помощи Employee.getTimetable
        Week.prototype.loadTimetable = function() {
            // проверяем, необходима ли загрузка данных
            if (this.timetableLoaded) {
                return $q.when();
            }
            
            // ищем минимальный и максимальный ID сотрудников
            var ids = this.employees.map(function(e) { return e.id });
            var minID = Math.min.apply(null, ids);
            var maxID = Math.max.apply(null, ids);
    
            // ограничиваем выборку ячеек номерами строк по предельным ID сотрудников
            // и столбцы по индексу колонки понедельника слева и +6 от него справа (7 колонок - 1 неделя)
            var options = { 'min-col': this.mondayIndex, 'max-col': this.mondayIndex + 6, 'min-row': minID, 'max-row': maxID };
            
            var week = this;
            return gSheets.getWorksheet(this.worksheet.id, options).then(
                function (result) {
                    // для каждой полученной ячейки
                    result.cells.forEach(function (cell) {
                        // проверяем существует ли сотрудник с номером строки ячейки
                        var employeeIndex = ids.indexOf(cell.position.row);
                        if (employeeIndex === -1 || employeeIndex >= week.employees.length || !week.employees[employeeIndex]) return true;
                        var employee = week.employees[employeeIndex];
        
                        // получаем номер дня недели по номеру столбца
                        var dayOfWeek = cell.position.col - week.mondayIndex;
                        if (dayOfWeek < 0 || dayOfWeek > 6) return true;
                        var date = week.getDays()[dayOfWeek];
        
                        // добавляем запись о смене сотрудника
                        employee.setShift(date, cell.content);
                    });
    
                week.timetableLoaded = true;
                return;
            });
        };
        
        return {
            // получает список рабочих недель за всё время
            getWeeks: function(callback) {
                if (weeks.length) {
                    return $q.when(weeks);
                }
                
                // запрашиваем документ GaGaGrafic с описанием листов
                return gSheets.getSpreadsheet(documentKey).then(
                    function (result) {
                        // формируем список недель
                        var mondayIndex = [];
                        result.worksheets.forEach(function (item) {
                            var week;
                            // в листе содержится две недели графика
                            // дату первой недели определяем по заголовку листа
                            var date = parseDate(item.title);
                            if (date === null) return true;
    
                            // пытаемся создать объект первой недели
                            try {
                                week = new Week(item, date);
                                weeks.push(week);
                                mondayIndex[week.monday.getTime()] = week;
                            } catch (e) { 
                                // если мы не смогли создать объект на основе
                                // имеющейся информации, тогда пропускаем лист
                                return true;
                            }
                            
                            // сдвигаем дату на неделю вперёд и
                            // пытаемся создать объект второй недели
                            date.setDate(date.getDate() + 7);
                            try {
                                week = new Week(item, date);
                                weeks.push(week);
                                mondayIndex[week.monday.getTime()] = week;
                            } catch (e) { return true; }
                        });
                        
                        // расставляем ссылки на prev и next weeks
                        weeks.forEach(function(week) {
                            var date;
                            
                            // prev
                            date = new Date(week.monday);
                            date.setDate(week.monday.getDate() - 7);
                            var prev = mondayIndex[date.getTime()];
                            if (prev) {
                                week.prev = prev;
                                prev.next = week;
                            }
                            
                            // next
                            date = new Date(week.monday);
                            date.setDate(week.monday.getDate() + 7);
                            var next = mondayIndex[date.getTime()];
                            if (next) {
                                week.next = next;
                                next.prev = week;
                            }
                            
                            // удаляем текущую неделю из индекса
                            // во избещание двойной работы
                            mondayIndex[week.monday.getTime()] = undefined;
                        });
                        
                        return weeks;
                    }
                );
            }
        };
    }];
});